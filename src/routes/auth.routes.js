const { Router } = require('express');
const router = Router();

const { signInService } = require('../services/signin.service');

router.post('/signin', signInService);

module.exports = router;
