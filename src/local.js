const app = require('./app');
const dotenv = require('dotenv');
dotenv.config();

port = process.env.PORT;

app.listen(port, () => {
  console.log(`Server on port ${port}`);
});
