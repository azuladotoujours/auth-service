const { dbQuery } = require('../../config/index');
const { getAccountByEmailQuery } = require('../queries/queries');

/**
 * Gets the account by email
 * @param {String} email
 * @returns {Object} account
 * @returns {Boolean}
 */
const getAccountByEmailDao = async (email) => {
  try {
    let account = await dbQuery.query(getAccountByEmailQuery, [email]);

    if (!account.rows[0]) {
      return false;
    } else {
      return account.rows[0];
    }
  } catch (e) {
    console.log(e);
    return false;
  }
};

module.exports = { getAccountByEmailDao };
