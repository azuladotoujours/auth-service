exports.getAccountByEmailQuery = 'SELECT * FROM accounts WHERE email = $1';

exports.createUserAccountQuery =
  'INSERT INTO accounts (email, user_id, hashed_password) VALUES ($1, $2, $3)';

exports.createRestaurantAccountQuery =
  'INSERT INTO accounts (email, restaurant_id, hashed_password) VALUES ($1, $2, $3)';
